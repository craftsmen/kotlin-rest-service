FROM adoptopenjdk:15-jdk-hotspot as gradle
COPY gradle/ gradle/
COPY gradlew .
COPY build.gradle.kts .
COPY settings.gradle.kts .
COPY gradle.properties .
RUN ./gradlew build
COPY . .
RUN ./gradlew build

FROM adoptopenjdk:15-jdk-hotspot
EXPOSE 8080
HEALTHCHECK CMD curl --fail http://localhost:8080/health
ENTRYPOINT ["kotlin-rest-service/bin/kotlin-rest-service"]
COPY --from=gradle /build/distributions/kotlin-rest-service.tar .
RUN tar -xf kotlin-rest-service.tar && rm kotlin-rest-service.tar
