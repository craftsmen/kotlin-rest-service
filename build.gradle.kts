plugins {
    application
	kotlin("jvm") version "1.4.30"
}

group = "nl.craftsmen.blog.kotlin"

application {
    mainClass.set("nl.craftsmen.blog.kotlin.NettyServer")
}

repositories {
    jcenter()
}

dependencies {
    implementation("org.glassfish.jersey.core:jersey-server:3.0.1")
    implementation("org.glassfish.jersey.inject:jersey-hk2:3.0.1")
    implementation("org.glassfish.jersey.containers:jersey-container-netty-http:3.0.1")
    implementation("org.glassfish.jersey.media:jersey-media-json-jackson:3.0.1")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.12.1")
    implementation("ch.qos.logback:logback-classic:1.2.3")
}