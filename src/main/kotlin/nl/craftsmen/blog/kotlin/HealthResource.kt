package nl.craftsmen.blog.kotlin

import jakarta.ws.rs.GET
import jakarta.ws.rs.Path
import jakarta.ws.rs.Produces
import jakarta.ws.rs.core.MediaType.APPLICATION_JSON

@Path("health")
@Produces(APPLICATION_JSON)
class HealthResource {

    @GET
    fun health() = "OK"
}