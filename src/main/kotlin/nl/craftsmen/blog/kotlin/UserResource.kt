package nl.craftsmen.blog.kotlin

import jakarta.ws.rs.DELETE
import jakarta.ws.rs.GET
import jakarta.ws.rs.POST
import jakarta.ws.rs.PUT
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import jakarta.ws.rs.Produces
import jakarta.ws.rs.core.MediaType.APPLICATION_JSON

@Path("users")
@Produces(APPLICATION_JSON)
open class UserResource {
    private val users = HashMap<String, User>()

    init {
        users += "Gerard" to User("Gerard", "secret", 25)
    }

    @GET @Path("{username}")
    fun getUser(@PathParam("username") username: String): User? {
        return users[username]
    }

    @POST
    fun createUser(user: User) {
        users += user.username to user
    }

    @PUT @Path("{username}")
    fun updateUser(@PathParam("username") username: String, user: User) {
        users -= username
        users += user.username to user
    }

    @DELETE @Path("{username}")
    fun deleteUser(@PathParam("username") username: String): User? {
        return users.remove(username)
    }
}