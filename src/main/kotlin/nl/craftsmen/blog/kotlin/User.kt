package nl.craftsmen.blog.kotlin

data class User(val username: String, val password: String, val age: Int)