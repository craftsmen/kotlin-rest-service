package nl.craftsmen.blog.kotlin

import jakarta.ws.rs.core.Application

class JerseyApplication : Application() {

    override fun getClasses(): MutableSet<Class<*>> = mutableSetOf(HealthResource::class.java, UserResource::class.java)

}